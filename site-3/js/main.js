
/*Слайдер */

$(document).ready(function(){

    $(".main-slider.owl-carousel").owlCarousel({
        items : 1,
        nav : true,
        navText : "",
        dots: true,
        loop : true,
        autoplay : true,
        autoplayHoverPause : true,
        fluidSpeed : 5000,
        autoplaySpeed : 1600,
        navSpeed : 1600,
        dotsSpeed : 1600,
        dragEndSpeed : 1600
    });



});




$('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    centerMode: false,
    focusOnSelect: true,
    responsive: [
        {
            breakpoint: 870,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
            }
        },
        {
            breakpoint: 500,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                centerMode: false,
                infinite: true,
            }
        }
    ]
});



$('.select-size').click(function(){
    $(this).parent().children('.select-size__content').toggle('fast');
    return false;
});



/*Проверка полей формы на валидность*/

function Formdata(data) {

    if (data.name != null && data.name.value.length < 3 )
    {
        $(data.name).css('border', '1px solid #e93832');
        $('.name-valid.valid').text('Введите Ваше имя');
        $('.valid').css('display', 'inline-block');
        return false;
    }

    if (data.mail != null && data.mail.value.length < 3 )
    {
        $(data.mail).css('border', '1px solid #e93832');
        $('.mail-valid.valid').text('Введите адрес электронной почты');
        $('.mail-valid.valid').css('display', 'inline-block');
        return false;
    }

    if (data.phone != null && data.phone.value.length < 3 )
    {
        $(data.phone).css('border', '1px solid #e93832');
        $('.phone-valid.valid').text('Введите Ваш номер телефона');
        $('.phone-valid.valid').css('display', 'inline-block');
        return false;
    }

    if (data.password != null && data.password.value.length < 3 )
    {
        $(data.password).css('border', '1px solid #e93832');
        $('.password-valid.valid').text('Введите пароль');
        $('.password-valid.valid').css('display', 'inline-block');
        return false;
    }

    var password = $(data.password).val();
    var password2 = $(data.password2).val();

    if (password != password2)
    {
        $(data.password2).css('border', '1px solid #e93832');
        $('.password2-valid.valid').text('Пароли не совпадают');
        $('.password2-valid.valid').css('display', 'inline-block');
        return false;
    }
}

jQuery(function($){
    $(document).mouseup(function (e){ // событие клика по веб-документу
        var div = $(".submit"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            $('.input-group').css('border', '1px solid #e1e4e9');
            $('.valid').css('display', 'none');
            $('.mail-valid.valid').css('display', 'inline-block');
        }
    });
});

function authformdata(data) {
    if (data.mail != null && data.mail.value.length < 3 )
    {
        $(data.mail).css('border', '1px solid #e93832');
        $('.mail-valid.valid').text('Введите адрес электронной почты');
        $('.mail-valid.valid').css('display', 'inline-block');
        return false;
    }

    if (data.password != null && data.password.value.length < 3 )
    {
        $(data.password).css('border', '1px solid #e93832');
        $('.password-valid.valid').text('Введите пароль');
        $('.password-valid.valid').css('display', 'inline-block');
        return false;
    }
}

function newPasswordFormdata(data) {
    if (data.password1 != null && data.password1.value.length < 3 )
    {
        $(data.password1).css('border', '1px solid #e93832');
        $('.password1-valid.valid').text('Введите станый пароль');
        $('.password1-valid.valid').css('display', 'inline-block');
        return false;
    }

    if (data.password2 != null && data.password2.value.length < 3 )
    {
        $(data.password2).css('border', '1px solid #e93832');
        $('.password2-valid.valid').text('Введите новый пароль');
        $('.password2-valid.valid').css('display', 'inline-block');
        return false;
    }

    var password = $(data.password2).val();
    var password2 = $(data.password3).val();

    if (password != password2)
    {
        $(data.password3).css('border', '1px solid #e93832');
        $('.password3-valid.valid').text('Пароли не совпадают');
        $('.password3-valid.valid').css('display', 'inline-block');
        return false;
    }
}

$(document).ready(function(){
    var pattern = /^[a-z0-9_-]+@[a-z0-9-]+\.[a-z]{2,6}$/i;
    var mail = $('#mail');

    mail.blur(function(){
        if(mail.val().search(pattern) == 0){
            $('.mail-valid.valid.valid').text('');
            $('#submit').attr('disabled', false);
            mail.removeClass('error').addClass('ok');
        }else{
            $('.mail-valid.valid').text('Некоррекный адрес электронной почты');
            $('.input-group.mail').css('border', '1px solid #e93832');
            $('#submit').attr('disabled', true);
            mail.addClass('ok');
        }

    });
});

//Маска ввода
$(function(){
    $(".phone").mask("+7 ( 999 ) 99-99-999");
});


/*Стилизация_Select/*/


$('.select').each(function(){
    // Variables
    var $this = $(this),
        selectOption = $this.find('option'),
        selectOptionLength = selectOption.length,
        dur = 400;

    $this.hide();
    $this.wrap('<div class="select"></div>');
    $('<div>',{
        class: 'select__gap',
        text: 'Размер'
    }).insertAfter($this);

    var selectGap = $this.next('.select__gap') ;
    $('<ul>',{
        class: 'select__list'
    }).insertAfter(selectGap);

    var selectList = selectGap.next('.select__list');
    for(var i = 0; i < selectOptionLength; i++){
        $('<li>',{
            class: 'select__item',
            html: $('<span>',{
                text: selectOption.eq(i).text()
            })
        })
            .attr('data-value', selectOption.eq(i).val())
            .appendTo(selectList);
    }
    var selectItem = selectList.find('li');

    selectList.slideUp(0);
    selectGap.on('click', function(){
        if(!$(this).hasClass('on')){
            $(this).addClass('on');
            selectList.slideDown(dur);

            selectItem.on('click', function(){
                var chooseItem = $(this).data('value');

                $('select').val(chooseItem).attr('selected', 'selected');
                selectGap.text($(this).find('span').text());

                selectList.slideUp(dur);
                selectGap.removeClass('on');
            });

        } else {
            $(this).removeClass('on');
            selectList.slideUp(dur);
        }
    });

});