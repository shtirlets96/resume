$('.js-modal-trigger').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        closeModals2();
        var modal = $(this).data('modal');
        $('.js-modal[data-modal="' + modal + '"]').addClass('modal--visible').find('.modal__content').scrollTop(0);
        $('body').css('overflow', 'hidden');
      });
      $('.js-modal').on('click', function () {
        closeModals2();
      });
	  $('.js-modal-close').on('click', function () {
        closeModals2();
      });
      $('.js-modal').children('.modal__content').on('click', function (e) {
        e.stopPropagation();
      });

      function closeModals2() {
        $('.js-modal').removeClass('modal--visible');
        $('body').css('overflow', 'visible');
		$('body').css('overflow-x', 'hidden');
      }