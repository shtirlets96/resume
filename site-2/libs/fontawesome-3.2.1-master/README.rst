#########################
Font Awesome 3.2.1 as NPM
#########################

About
=====
This is Font Awesome 3.2.1 with a ``package.json`` to be NPM package compatible.

Install
=======
::

    yarn add https://github.com/ip-tools/fontawesome-3.2.1

----

Have fun!
